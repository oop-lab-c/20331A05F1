class Sender
{
    public void send(String msg)
    {
        System.out.println("Sending\t"  + msg );
        System.out.println("\n" + msg + "Sent");
    }
}

class ThreadedSend extends Thread
{
    private String msg;
    Sender  sender;
    ThreadedSend(String m,  Sender obj)
    {
        msg = m;
        sender = obj;
    }
 
    public void run()
    {
        synchronized(sender)
        {
            sender.send(msg);
        }
    }
}
class Main{
    public static void main(String args[])
    {
        Sender send = new Sender();
        ThreadedSend S1 =
            new ThreadedSend( " Hi " , send );
        ThreadedSend S2 =
            new ThreadedSend( " Bye " , send );
 
        S1.start();
        S2.start();
     }
}