class MyThread implements Runnable{
    public void run() {
        for(int i=0;i<5;i++){
            System.out.println(i);
        }
    }
}
class Main{
    public static void main(String[] args){
        MyThread t1 = new MyThread();
        Thread t2= new Thread(t1);
        t2.start();
        for(int i=0;i<5;i++){
            System.out.println(i);
        }
    }
}