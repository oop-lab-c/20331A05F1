//Write two header files in C++ as :
//1. boxArea.h - void boxArea(float length, float width).
//2. boxVolume.h - void boxVolume(float length, float width, float height)
//Also using #include, #ifdef and #ifndef write a C++ program to display area and volume of 
//box with user input.

#include<iostream>
#include "boxArea.h"    //including header file
#include "boxVolume.h"
#define Area            
#define Volume
using namespace std;
int main(int argc, char const *argv[])
{
    cout<<"Enter the length, width, height of a box = "<<endl;
    float l,w,h;
    cin>>l>>w>>h;
    #ifdef Area         // if area defines execute area block
        boxArea(l,w);
    #ifndef V           //contrast to #ifdef
        boxVolume(l,w,h);
    #endif
    #endif
    return 0;
}
