/*Using a constructor and destructor in C++, Write a C++ class 'Student' and assign the 
following :
1. default constructor (string collegeName = "MVGR", int collegeCode=33)
2. parameterized constructor (string fullName, double semPerentage)
Also create the objects respectivelty asd display their values.
*/
#include<iostream>
using namespace std;
class Student{
    // By default private
    string collegeName, name;
    int collegeCode, rollNo;
    double semPercentage;
    public:
    Student(){
        collegeName = "MVGR", 
        collegeCode = 33;
    }
    Student(double sp, int r, string name){
        semPercentage = sp;
        rollNo = r;
        this->name = name;
    }
    ~Student(){     //constructor cannot be overloaded . declare last.
        cout<<"Constructor destroyed"<<endl;
    }    
    void display(){
        cout<<"College Name = "<<collegeName<<endl;
        cout<<"College Code = "<<collegeCode<<endl;
    }
    void diplay2(){
        cout<<"Full name = "<<name<<endl;
        cout<<"Roll number = "<<rollNo<<endl;
        cout<<"Sem precentage = "<<semPercentage<<endl;
    }
    // we can place constructor here also    
};
int main(int argc, char const *argv[])
{
    int r; double p ; char n[50];
    cout<<"Enter Full name = ";
    cin.get(n,50);           //getline(cin,n) if string 
    cout<<"Enter Roll number = ";
    cin>>r;
    cout<<"Enter Sem precentage = ";
    cin>>p;
    Student obj;
    Student obj2 = Student(p,r,n);
    obj2.diplay2();
    obj.display();
    return 0;
}
