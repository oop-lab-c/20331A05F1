/*Write a C++ class 'Box' and its members as the following :
* void boxArea(float length, float width) : as a member function defiend inside class
* void boxVolume(float length, float width, float height) : as a member function defined 
oustide class
* void displayBoxDimensions() : as a friend function
* void displayWelcomeMessage() : as an inline function
Note: Take the input from the user*/

#include<iostream>
using namespace std;
class Box{
    float length, width, height;
    public:
    void set(float length, float width, float height){
        this->length = length;
        this->width = width;
        this->height = height;
    }    
    void boxArea(float length, float width){
        cout<<"The Area of box = "<<length*width<<endl;
    }
    void  boxVolume(float length, float width, float height);
    friend void displayBoxDimensions(Box obj);
};
inline void displayWelcomeMessage(){
    cout<<"Hi welcome"<<endl;
}
void Box ::  boxVolume(float length, float width, float height){
    cout<<"The volume of box = "<<length*width*height<<endl;
}
void  displayBoxDimensions(Box obj){
    cout<<"Dimensions = "<<"\nlength = "<<obj.length<<"\nwidth = "<<obj.width<<"\nheight = "<<obj.height<<endl;
 }
 int main(int argc, char const *argv[])
 {
     Box obj;
     displayWelcomeMessage();
     float l,w,h;
     cout<<"Enter the length, width, height = ";
     cin>>l>>w>>h;     
     obj.set(l,w,h);
     obj.boxArea(l,w);
     obj.boxVolume(l,w,h);
     displayBoxDimensions(obj);     
     return 0;
 }
 