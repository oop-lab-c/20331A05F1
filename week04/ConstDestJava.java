public class ConstDestJava {
    String collegeName, name;
    int collegeCode, rollNo;
    double semPercentage;
    ConstDestJava(){
        collegeName = "MVGR";
        collegeCode = 33;
        name = "Chandan Lohit";
        rollNo = 41;
        semPercentage = 90;
    }
    void display(){
        System.out.println("Full name = "+name);
        System.out.println("Roll no = "+ rollNo);
        System.out.println("sem percentage = "+semPercentage);
        System.out.println("college code = "+collegeCode);
        System.out.println("college name = "+collegeName);
    }
    protected void finalize(){
        System.out.println("Object is destroyed");
    }

public static void main(String[] args) {
    ConstDestJava obj = new ConstDestJava();
    obj.display();
    obj = null;
    System.gc();
}
}