/*Using a constructor and destructor in Java, Write a Java class 'Student' and assign the 
following :
1. default constructor (string collegeName = "MVGR", int collegeCode=33)
2. parameterized constructor (string fullName, double semPerentage)
Also create the objects respectivelty asd display their values.
*/
import java.util.Scanner;
class ParamConstJava{
    String collegeName, name;
    int collegeCode, rollNo;
    double semPercentage;
    ParamConstJava(){
        collegeName = "MVGR";
        collegeCode = 33;
    }
    ParamConstJava(String name, int r, double sem){ // String S should be capital
        this.name = name;
        rollNo = r;
        semPercentage = sem;
    }
    void display (){
        System.out.println("college code = "+collegeCode);
        System.out.println("college name = "+collegeName);        
    }
    void display1(){
        System.out.println("Full name = "+name);
        System.out.println("Roll no = "+ rollNo);
        System.out.println("sem percentage = "+semPercentage);        
    }
    protected void finalize(){
        System.out.println("Object is destroyed");
    }
    public static void main(String[] args) {
        ParamConstJava s = new ParamConstJava();
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the Name ");
        String name = sc.nextLine();
        System.err.println("Enter the rollno ");
        int r = sc.nextInt();
        System.out.println("Enter the sem precentage" );
        double sem = sc.nextDouble();
        ParamConstJava s1 = new ParamConstJava(name,r,sem);
        s.display();
        s1.display1();
        s = s1;
        System.gc();
        sc.close(); //resourse leak error closed.
    }
}
