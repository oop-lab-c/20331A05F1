//Write a C++ function to check whether a number is even or odd.
#include<iostream>
using namespace std;
void evenOdd(int a){
    if (a%2 == 0){
        cout<<a<<" is even"<<endl;
    }
    else{
        cout<<a<<" is odd"<<endl;
    }
}
int main(int argc, char const *argv[])
{
    int a;
    cout<<"Enter the number = ";
    cin>>a;
    evenOdd(a);
    return 0;
}