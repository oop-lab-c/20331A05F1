package week1;
import java.util.Scanner;

class HelloUserJava{
    public static void main(String[] args) {
        Scanner sc = new Scanner(System.in);
        System.out.println("Enter the user name :");
        String name = sc.nextLine();
        System.out.println("Hello "+ name);
    }
}
