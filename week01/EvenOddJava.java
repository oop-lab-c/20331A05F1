package week1;
import java.util.Scanner;
public class EvenOddJava {
    static int a = 10;
    public static void evenOdd(int a){
        if (a%2 == 0){
            System.out.println(a+" is even");
        }
        else{
            System.out.println(a+" is odd");
        }
    }
    public static void main(String[] args) {
        System.out.println("Enter the number = ");
        Scanner sc = new Scanner(System.in);
        int a = sc.nextInt();
        evenOdd(a);
        sc.close();
    }
}
